# spring-microservices

Simple Springboot Microservices

- Spring Vault
- Service Discovery
- Load Balancing
- Spring Actuator

## List Service
-  branch discovery to coverage service discovery(8761)
-  branch book to coverage service book(8001)
-  branch catalog for consume service book(8002)