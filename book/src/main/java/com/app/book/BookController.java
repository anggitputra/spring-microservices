package com.app.book;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {

    @RequestMapping("/")
    public String home(){
        return "Home Book Application";
    }

    @RequestMapping("/book")
    public String getBook(){
        return "The Jungle Land";
    }
}
