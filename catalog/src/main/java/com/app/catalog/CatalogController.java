package com.app.catalog;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CatalogController {

    @Autowired
    private EurekaClient client;

    @RequestMapping("/")
    public String getHome(){
        String response = "";
//        String url= "http://localhost:8080/";
        InstanceInfo instanceInfo = client.getNextServerFromEureka("fx-app",false);
        RestTemplate restTemplate = new RestTemplate();
        String url = instanceInfo.getHomePageUrl();
        response = restTemplate.getForObject(url,String.class);
        return "Welcome to book catalog and " + response;
    }
    @RequestMapping("/catalog")
    public String getBook() {
        String response = "";
//        String url = "http://localhost:8001/book";
        InstanceInfo instanceInfo = client.getNextServerFromEureka("fx-app",false);
        RestTemplate restTemplate = new RestTemplate();
        String url = instanceInfo.getHomePageUrl()+"/book";
        response = restTemplate.getForObject(url, String.class);
        return "Book name :" + response;
    }
}
